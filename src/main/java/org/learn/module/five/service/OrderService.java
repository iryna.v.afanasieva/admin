package org.learn.module.five.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import org.learn.module.five.Producer;

import javax.jms.JMSException;
import java.io.IOException;
import java.util.function.Function;

public class OrderService {
    public static final int TOTAL_THRESHOLD = 2;
    public static final int QTY_THRESHOLD = 4;
    public static final float VOLUME_THRESHOLD = 2.0f;
    private static final ObjectMapper mapper = new ObjectMapper();
    private final Producer acceptedProducer;
    private final Producer rejectedProducer;

    public OrderService(Producer acceptedProducer, Producer rejectedProducer) {
        if (acceptedProducer == null || rejectedProducer == null) {
            throw new RuntimeException("Producer(s) not found");
        }
        this.acceptedProducer = acceptedProducer;
        this.rejectedProducer = rejectedProducer;
    }

    public void process(String bodyMessage) throws JMSException, IOException {
        if (bodyMessage == null) {
            throw new IllegalArgumentException("No body message.");
        }
        if (isAccepted(convertTextMessage(bodyMessage))) {
            acceptedProducer.send(bodyMessage);
            System.out.printf("Message accepted:\n\t%s\n", bodyMessage);
        } else {
            rejectedProducer.send(bodyMessage);
            System.out.printf("Message rejected:\n\t%s\n", bodyMessage);
        }
    }

    private JsonNode convertTextMessage(String bodyMessage) throws IOException {
        return mapper.readTree(bodyMessage);
    }

    private boolean isAccepted(JsonNode jsonNode) {
        if (jsonNode == null) {
            throw new IllegalArgumentException("No data.");
        }
        final int totalQty = getJsonNodeValue(jsonNode, "totalQty", JsonNode::asInt);
        final int total = getJsonNodeValue(jsonNode, "total", JsonNode::asInt);
        final double totalVolume = getJsonNodeValue(jsonNode, "totalVolume", JsonNode::asDouble);

        if (totalQty > QTY_THRESHOLD) {
            System.out.printf("\nQTY_THRESHOLD: %s > %s\n\n", totalQty, QTY_THRESHOLD);
            return false;
        }
        if (totalVolume > VOLUME_THRESHOLD) {
            System.out.printf("\nVOLUME_THRESHOLD: %s > %s\n\n", totalVolume, VOLUME_THRESHOLD);
            return false;
        }
        if (total > TOTAL_THRESHOLD) {
            System.out.printf("\nTOTAL_THRESHOLD: %s > %s\n\n", total, TOTAL_THRESHOLD);
            return false;
        }
        return true;
    }

    private <T> T getJsonNodeValue(JsonNode jsonNode, String fieldName, Function<JsonNode, T> extractor) {
        if (jsonNode == null) {
            throw new IllegalArgumentException("JsonNode is not found");
        }
        checkField(jsonNode, fieldName);
        return extractor.apply(jsonNode.get(fieldName));
    }

    private void checkField(JsonNode jsonNode, String fieldName) {
        if (jsonNode == null) {
            throw new IllegalArgumentException("JsonNode is not found");
        }
        if (fieldName == null || fieldName.isBlank()) {
            throw new IllegalArgumentException("FieldName is not found");
        }
        JsonNodeType expectedType = JsonNodeType.NUMBER;
        if (!jsonNode.has(fieldName)) {
            throw new IllegalArgumentException(String.format("No field %s found", fieldName));
        }
        final var actualType = jsonNode.get(fieldName).getNodeType();
        if (!expectedType.equals(actualType)) {
            throw new IllegalArgumentException(
                String.format(
                    "Type mismatch detected: %s instead of %s",
                    actualType,
                    expectedType));
        }
    }
}
