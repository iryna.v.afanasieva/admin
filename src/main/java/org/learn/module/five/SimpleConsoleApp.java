package org.learn.module.five;

import org.learn.module.five.configuration.Connection;
import org.learn.module.five.service.OrderService;

import javax.jms.JMSException;
import java.io.IOException;

public class SimpleConsoleApp {

    public static void main(String[] args) throws JMSException, IOException {
        try(
            Connection connection = new Connection();
            Consumer consumer = new Consumer(connection, "order-queue");
            Producer acceptedProducer = new Producer(connection, "accepted-queue");
            Producer rejectedProducer = new Producer(connection, "rejected-queue");
        ) {
            OrderService orderService = new OrderService(acceptedProducer, rejectedProducer);
            while(true) {
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                final var message =  consumer.receive();
                if (message == null) {
                    System.out.println("Sorry no message");
                } else {
                    System.out.printf(
                        "Received message\n \tid:\n \t\t%s,\n\tvalue:\n \t\t%s\n",
                        message.getMessageId(),
                        message.getText());
                    try {
                        orderService.process(message.getText());
                        message.acknowledge();
                        consumer.commit();
                    } catch (Exception exception) {
                        System.out.println("Something went wrong: " + exception.getMessage());
                    }
                }
            }
        }
    }

}
