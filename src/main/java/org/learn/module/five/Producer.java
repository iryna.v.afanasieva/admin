package org.learn.module.five;

import org.apache.activemq.command.ActiveMQQueue;
import org.learn.module.five.configuration.Connection;

import javax.jms.IllegalStateException;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

public class Producer implements AutoCloseable {
    private final Session session;
    private final MessageProducer producer;

    public Producer(Connection connection, String destination) throws JMSException {
        if (connection == null) {
            throw new RuntimeException("Connection is not found");
        }
        if (destination == null) {
            throw new RuntimeException("Destination is not found");
        }
        var mqQueue = new ActiveMQQueue(destination);
        this.session = connection.createSession();
        if (this.session == null) {
            throw new RuntimeException("Session is not found");
        }
        this.producer = this.session.createProducer(mqQueue);
        if (this.producer == null) {
            throw new RuntimeException("Producer is not found");
        }
    }

    public void send(String message) throws JMSException {
        if (message == null || message.isBlank()) {
            throw new IllegalStateException("Message is not found");
        }
        if (producer == null || session == null) {
            throw new RuntimeException("Producer or/and session is/are null");
        }
        try {
            producer.send(session.createTextMessage(message));
        } finally {
            session.commit();
        }
    }

    @Override
    public void close() throws JMSException {
        if (producer == null) {
            throw new RuntimeException("Producer is not found");
        }
        producer.close();
        if (session == null) {
            throw new RuntimeException("Session is not found");
        }
        session.close();
    }
}
