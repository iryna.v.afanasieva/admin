package org.learn.module.five;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.learn.module.five.configuration.Connection;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;

public class Consumer implements AutoCloseable {
    private final Session session;
    private final MessageConsumer consumer;

    public Consumer(Connection connection, String destination) throws JMSException {
        if (connection == null) {
            throw new RuntimeException("Connection is not found");
        }
        if (destination == null) {
            throw new RuntimeException("Destination is not found");
        }
        var mqQueue = new ActiveMQQueue(destination);
        this.session = connection.createSession();
        if (this.session == null) {
            throw new RuntimeException("Session is not found");
        }
        this.consumer = this.session.createConsumer(mqQueue);
        if (this.consumer == null) {
            throw new RuntimeException("Consumer is not found");
        }
    }

    public ActiveMQTextMessage receive() throws JMSException {
        if (consumer == null) {
            throw new RuntimeException("Consumer is not found");
        }
        final var message = consumer.receive();
        if (message instanceof ActiveMQTextMessage) {
            return (ActiveMQTextMessage) message;
        }
        return null;
   }

    @Override
    public void close() throws JMSException {
        if (consumer == null) {
            throw new RuntimeException("Consumer is not found");
        }
        consumer.close();
        if (session == null) {
            throw new RuntimeException("Session is not found");
        }
        session.close();
    }

    public void commit() throws JMSException {
        if (session == null) {
            throw new RuntimeException("Session is not found");
        }
        session.commit();
    }
}
